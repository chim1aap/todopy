# Configuration

Configuration is done through a `config.yaml` file. 
todotree searches for the file in the following locations in order:

- The path specified with the `--config-file` parameters.
  - This is useful when you want to have the config file at a different location. It can also be used to define an alias in your shell, 
- `$XDG_CONFIG_HOME/todotree/config.yaml`, which defaults to `~/.config/todotree/config.yaml` on linux.
  - This is the official location according to the XDG specifications.
- `$XDG_CONFIG_DATA/todotree/config.yaml`, which defaults to `~/.local/share/todotree/config.yaml` on linux.
  - This is a very useful location if you want to have the configuration stored in the same git repository 
as the rest of the files.
- The default parameters.

The default parameters are documented in ./examples/config.yaml file.

### Powershell tip

A useful way to alias it in PowerShell is to add the following snippet to your `$PROFILE` script. You can open the file with `notepad $PROFILE`.

```{pwsh}
Function todo-tree {
    todotree --config-file "C:/path/to/config.yaml" $args
}
Set-Alias todo todo-tree
```

