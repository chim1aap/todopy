# Addons

Just like the original `todo.sh`, todotree allows you to run addons.
The folder which contains the addons is defined by `path.addons` in `config.yaml`.
An addon can be run with `todotree addons <addon-name>`.

I have used [ice_recur](https://github.com/smartlitchi/ice-recur) for years, which is why I build this addon system in the first place.