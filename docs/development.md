# Development

This document is intented for those who wish to help develop this marvelous application.

The Main.py file contains the click interfaces for running it on the command line.
Each command should run a Command in the Command subfolder.

A command will run all TaskManagers, depending on which are needed.
A TaskManager is a glorified list of Tasks with some extra functions for manipulating tasks.
A Task is a single line of text in the todo.txt or done.txt. It represents a single task that can be ticked off.
