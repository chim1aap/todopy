# Due dates in todotree

There are always tasks which have a due date that they need to be completed before.
In Todotree there are three different keywords:

- `due`: intended for single use. For example getting married. You can set these using the `schedule` command or by 
adding `due:year-month-day` to a task.
- `duy`: intended for yearly recurring due dates. 
An example of this is doing your taxes. 
You can set these by adding `duy:month-day` to a task.
If the duy date is near the beginning of the year, the date is taking at least one month before.
For example, if the task is `(B) celebrate new years duy:01-01`. Then in December of the year before the due date is already shown in the `due` command. 
- `dum`: intended for monthly recurring due dates.
An example would be to send the invoices.
You can set these by adding `dum:day` to a task.
If the dum date is near the beginning of the month, the date is taken at least one week before.
For example, if the task is `(B) send invoices dum:01`. Then the week before that in the previous month it is already showing up in the `due` command.
  