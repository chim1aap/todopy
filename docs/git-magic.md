# Git magic

This is just a showcase on how powerful git actually is. It is in no way  


### The situation

I am currently working on a Windows computer with rainmeter. I also like to work in the wsl environment. 
Thus, I want to have todotree available on both PowerShell and bash, and they need to sync between each other.
Also, I do not want to use a remote repository such as GitHub, Gitlab, or gitea. Everything has to be local.

### The solution

The solution only uses bare git and todotree, and does not need any external tool.

We will have three repositories. 
These paths are changed for readability, they can be anywhere on a filesystem. 


1. windows-todotree: 
    - Located at C:\todotree
    - Used by PowerShell (using the script at ./configuration.md)
2. linux-todotree:
    - Located at /home/todotree
    - Used by bash. 
3. Remote
    - Located at /git/todotree

To set it up, do the following steps. modify the paths to suit your situation.

1. Configure a bare repo at Remote: 

```{sh}
mkdir -p /git/todotree
cd /git/todotree
git init --bare
```

2. Configure linux-todotree:

```{sh}
mkdir /home/todotree
cd /home/todotree
touch todo.txt done.txt config.yaml
git init
git add *
git commit -m "First commit"
git remote add origin /git/todotree
git push
``` 

3. Configure window-todotree

```{pwsh}
Set-Location C:\
git clone \\wsl$\Ubuntu-22.04\home\todotree
git push --set-upstream origin main 
```