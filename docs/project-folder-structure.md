# The project folder feature

The project folder feature can be enabled by setting 
the `enable_project_folder` key in the `main` section of the configuration to `True`.

Then the folder in which your projects are stored is configured with 
the `project_folder` key in the `paths` section of the configuration file.

The feature populates the projects tree with the sub-folders in that folder. 
Each sub-folder is a single project. Any files in that folder are ignored.
This allows you to keep track of projects which do not have todos at the moment and gives you an 
opportunity to think of the next step in the project. 

The system assumes that there always is a next step or task that could be done for a certain project.
However, if it happens that you are waiting for somebody and are completely stuck. 
In this case, you can hide the project by adding a reminder in your `todo.txt` to check on the blocking thing.
Add a schedule to remove the project for some time, but that will always resurface in time to remind you.
For example:
```todo
(B) +project Check on *person* t:2022-02-02
```

----

Inspiration for this feature is drawn from the book `Getting things done` by David Ellen.
In that book, the assumption is made that every project always has a task that can be done _right now_. 
