# Recur
Todotree has a command called `recur`, which adds tasks based on a `recur.txt` file.
It is heavily inspired by [Remember the milk](https://www.rememberthemilk.com) and [ice_recur](https://github.com/smartlitchi/ice-recur). 
Those two have more features than this recurrence implementation, so if you want to use those, you can do so using [addons](./addons.md). 

## Usage

`todotree recur` is intended to be run each day, in order to refresh the task list.
However, if days are missed they will be added as well. `recur` works very nicely with `duy` and `dum` due dates,
for recurring deadlines.

The simplest way to be sure that recur runs sufficiently often that the recurring tasks are added is to add the command
to your startup script, i.e. to `~/.profile` in bash or `$PROFILE` in PowerShell. 
The tasks will then be added when the shell is started. This only works when you have a single main machine to access the tasks.

Below are only a few ways this command can be integrated when you work on multiple devices. 
If you know of another awesome way, feel free to add it via a merge request.

### Gitlab CI example

The following snippet can be used to run a Gitlab CI job with todotree recur.
```yaml
stages:
  - build

Todotree recur:
  image: python:3.11
  stage: build
  script:
    # Install todotree.
    - pip install todotree
    # run todotree.
    - todotree recur
```

### Gitea Action / GitHub Action

The following snippet runs a Gitea action each day in the morning. 

```yaml
name: Todotree recur action
run-name: Todotree Recur
on:
  schedule:
    - cron: '0 5 * * *' # Every day on 5 am.

jobs:
  TodoTree-Recur:
    runs-on: ubuntu-latest
    steps:
      - name: Check out repository code
        uses: actions/checkout@v3
      # Copy to default location (checkout is not allowed to do so).
      - run: mkdir -p /root/.local/share/todotree 
      - run: cp -r ./ /root/.local/share/todotree
      # Make sure todotree is installed.
      - run: pip install todotree
      # Add the recurring tasks.
      - run: todotree recur
```

### Cron tab example

Cron is a simple but very powerful scheduler in linux systems. 
This example runs each day on half past eight.
```cronexp
30 8 * * * todotree recur
```


## recur.txt reference

`recur.txt` has a line for each task that have to be added periodically. 
Each line starts with a start-date, an optional end date and an interval seperated by a ';'.
Then a `-` dash is inserted and after that the task is defined.
Lines starting with a `#` hashtag are ignored.

A working example is as follows:
```
# <start-date> ; <end-date> ; <interval> - <task>
2020-01-01 ; 2050-01-01 ; Daily - (A) Fill recur.txt
2020-01-01 ;; Yearly - (C) Celebrate new year.
2024-06-06 ;; Never - This task will only be added on 6th of june, 2024
2023-06-06 ;; Daily - This task is added each day.
2023-11-15 ;; Weekly - This task is added each week on a wednesday.
2023-06-06 ;; Monthly - This task is added on the 6th of each month.
2023-06-06 ;; Yearly - This task is added on the 6th of june each year. 
```

Intervals can be one of the following:

- Never: Runs only on the start and end date
- Daily: Runs each day.
- Weekly: Runs each week.
- Monthly: Runs each month on the day of the start-date.
- Yearly: Runs each year on the day-month of the start-date.

An example which is particularly useful is in combination with `dum` due dates. 
Below example adds a reminder on the 27th that the time sheets need to be filled in before the first of the next month. 
```
2000-01-27 ;; Monthly - (B) Submit monthly time sheets dum:01
```

Which can be amended very easily if you know you will quit this job in 2025 to travel the world for a couple of months.
You just add the end date.
```
2000-01-27 ; 2025-05-05 ; Monthly - (B) Submit monthly time sheets dum:01
```


