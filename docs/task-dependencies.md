# Task dependencies

Sometimes there is a task which cannot be done yet because there is another task which has to be completed first.

For example, you may have the following two tasks.

- Vacuum the room bl:example
- mop the floor by:example

You can only mop the floor if it is vacuumed first.

You can tell todotree this dependency with the `bl` and `by` keywords.
The `bl` keyword is given to tasks which are _bl_ocking other tasks. 
The `by` keyword is given to tasks which are blocked _by_ other tasks.
The `example` word is just an identifier. This is to identify a unique relation.

The dependency is a many-to-many relation. This means that one task can block many other tasks, 
or that a task may be blocked by many other tasks, or both. 
