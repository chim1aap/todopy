from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Errors.DoneFileNotFoundError import DoneFileNotFoundError
from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError
from todotree.Main import root
from todotree.Managers.AbstractManager import AbstractManager
from todotree.Managers.DoneManager import DoneManager


class TestMainFileNotFound:
    """
    Test all the ways that Files could not be found.

    FUTURE: move these to their separate integration test classes.
    """

    def test_no_file_found_revive(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `revive`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise DoneFileNotFoundError("Not Found")
        monkeypatch.setattr(DoneManager, "import_tasks", mock_open)
        runner_done = CliRunner()
        with runner_done.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_done = runner_done.invoke(root, "revive 1")

        # Assert
        assert result_done.exit_code == 1
        assert result_done.output[0:58] == "!!!The done.txt could not be found.\n!!!It searched at the "


    def test_no_file_found_addx(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `addx`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "addx 'some task'")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The done.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_do_todo(self, monkeypatch):
        """
        Test that an error is returned when todo.txt is not found using `do`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "do 1")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_do_done(self, monkeypatch):
        """
        Test that an error is returned when done.txt is not found using `do`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise DoneFileNotFoundError("Not Found")
        monkeypatch.setattr(DoneManager, "import_tasks", mock_open)
        runner_done = CliRunner()

        # Act.
        with runner_done.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="Existing Task\n")
            result_done = runner_done.invoke(root, f"do 1")

        # Assert
        assert result_done.exit_code == 1
        assert result_done.output[0:58] == "!!!The done.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_append(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `append`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "append 1 'some text'")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_list_done(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `list_done`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise DoneFileNotFoundError("Not Found")
        monkeypatch.setattr(DoneManager, "import_tasks", mock_open)
        runner_done = CliRunner()
        with runner_done.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_done = runner_done.invoke(root, "list_done")

        # Assert
        assert result_done.exit_code == 1
        assert result_done.output[0:58] == "!!!The done.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_print_raw(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `priority`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "print_raw")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_no_file_found_schedule(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `schedule`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "schedule 1 '2055-01-01'")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "
