from pathlib import Path

import pytest
import xdg_base_dirs
from git import Repo


def setup_files(monkeypatch: pytest.MonkeyPatch, /, *,
                recur_content: str | None = None, todotxt_content: str | None = None,
                donetxt_content: str | None = None, configyaml_content: str | None = None,
                stale_content: str | None = None):
    """
    Setup of the files.
    :param monkeypatch: monkeypatch of the calling function.
    content params:
     - If None, do not create the file,
     - if "", Create the file, but do not add anything
     - if str, Create the file and add the str contents to it.
    :param configyaml_content: content param.
    :param stale_content: content param.
    :param donetxt_content: content param.
    :param recur_content: content param.
    :param todotxt_content: content param.
    :return: A tuple, useful for assertions later.
        - Path of todo.txt
        - Path of done.txt
        - Path of config.yaml
        - Path of stale.txt
    """
    cwd = Path(".").resolve()
    # File / Folder setup.
    todotree_folder = cwd / "xdg" / "data" / "todotree"
    todotree_folder.mkdir(parents=True, exist_ok=True)

    if recur_content is not None:
        recur_path = todotree_folder / "recur.txt"
        recur_path.write_text(recur_content)
    if todotxt_content is not None:
        todo_txt = todotree_folder / "todo.txt"
        todo_txt.write_text(todotxt_content)
    else:
        todo_txt = None

    if donetxt_content is not None:
        done_txt = todotree_folder / "done.txt"
        done_txt.write_text(donetxt_content)
    else:
        done_txt = None

    if configyaml_content is not None:
        config_yaml = todotree_folder / "config.yaml"
        config_yaml.write_text(configyaml_content)
    else:
        config_yaml = None

    if stale_content is not None:
        stale_txt = todotree_folder / "stale.txt"
        stale_txt.write_text(stale_content)
    else:
        stale_txt = None

    # Monkeypatch xdg.
    def mock_data_home():
        p = Path(cwd) / "xdg" / "data"
        p.mkdir(parents=True, exist_ok=True)
        return p

    def mock_config_home():
        p = Path(cwd) / "xdg" / "config"
        p.mkdir(parents=True, exist_ok=True)
        return p

    monkeypatch.setattr(xdg_base_dirs, "xdg_data_home", mock_data_home)
    monkeypatch.setattr(xdg_base_dirs, "xdg_config_home", mock_config_home)
    return todo_txt, done_txt, config_yaml, stale_txt


def setup_git(td_remote: Path, td_local: Path):
    """
    Set up the local and remote git repositories.
    :param td_remote: Remote Temp Directory
    :param td_local: Local (TodoTree's) Temp Directory
    """
    remote_repo = Repo.init(td_remote, bare=True)

    local_repo = Repo.init(td_local)
    local_repo.index.add("*")
    local_repo.index.commit(message="first commit.")
    local_repo.create_head("main")
    local_repo.heads.main.checkout()

    local_repo.create_remote("origin", str(td_remote))
    local_repo.git.push("--set-upstream", "origin", "main")

    return local_repo, remote_repo


def setup_git_local_only(td_local: Path):
    """
    Set up the local git repo only.
    :param td_local: Local (TodoTree's) Temp Directory
    """

    local_repo = Repo.init(td_local)
    local_repo.index.add("*")
    local_repo.index.commit(message="first commit.")
    local_repo.create_head("main")
    local_repo.heads.main.checkout()

    return local_repo
