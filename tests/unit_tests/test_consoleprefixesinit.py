import sys
from io import StringIO

import click

from todotree.Config.ConsolePrefixes import ConsolePrefixes
from todotree.Config.ConsolePrefixesInit import ConsolePrefixesInit


class TestConsolePrefixesInit:
    def test_empty(self):
        """Test that ConsolePrefixesInit can initialize without exceptions."""
        ConsolePrefixesInit(True, "good", "warn", "error", "green", "yellow", "red")

    def test_from_base_class(self):
        """That a ConsolePrefixesInit can be initialized from a ConsolePrefix class."""
        base = ConsolePrefixes(True, "good", "warn", "error", "green", "yellow", "red")
        ct = ConsolePrefixesInit.from_console_prefixes(base)

        assert ct.info_prefix == base.info_prefix
        assert ct.warning_prefix == base.warning_prefix
        assert ct.error_prefix == base.error_prefix
        assert ct.info_color == base.info_color
        assert ct.warn_color == base.warn_color
        assert ct.error_color == base.error_color
        assert ct.enable_colors == base.enable_colors

    def test_confirm(self, monkeypatch):
        """Test that the confirm method works."""

        # Arrange.
        def mock_confirm(*args, **kwargs):
            print(kwargs.pop("text"))
            return True

        monkeypatch.setattr(click, "confirm", mock_confirm)
        base = ConsolePrefixesInit(True, "good", "warn", "error", "green", "yellow", "red")

        # Act.
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        result = base.confirm(text="test")
        sys.stdout = sys.__stdout__  # Reset redirect.

        # Assert
        assert result
        assert captured_output.getvalue() == "warntest\n"

    def test_prompt(self, monkeypatch):
        """Test that the prompt method works."""
        # Arrange.
        def mock_confirm(*args, **kwargs):
            print(kwargs.pop("text"))
            return True

        monkeypatch.setattr(click, "prompt", mock_confirm)
        base = ConsolePrefixesInit(True, "good", "warn", "error", "green", "yellow", "red")

        # Act.
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        result = base.prompt(text="test")
        sys.stdout = sys.__stdout__  # Reset redirect.

        # Assert
        assert result
        assert captured_output.getvalue() == "warntest\n"

    def test_prompt_menu(self, monkeypatch):
        """Test that the prompt_menu method works."""
        # Arrange.
        question = "Question?"
        answers = ["Answer1", "Answer2", "Answer3", "Answer4"]
        base = ConsolePrefixesInit(True, "good", "warn", "error", "green", "yellow", "red")

        def mock_prompt(*args, **kwargs):
            print(kwargs.pop("text"))
            return 2

        def mock_confirm(*args, **kwargs):
            return True

        monkeypatch.setattr(ConsolePrefixesInit, "prompt", mock_prompt)
        monkeypatch.setattr(ConsolePrefixesInit, "confirm", mock_confirm)

        # Act.
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        result = base.prompt_menu(question, answers)
        sys.stdout = sys.__stdout__  # Reset redirect.

        # Assert.
        assert result == 2
        assert captured_output.getvalue() == """Question?
  [0] Answer1
  [1] Answer2
  [2] Answer3
  [3] Answer4

"""

    def test_prompt_menu_custom_option(self, monkeypatch):
        """Test that the prompt_menu method with a custom option works."""

        # Arrange.
        question = "Question?"
        answers = ["Answer1", "Answer2", "Answer3", "Answer4"]
        custom_answer = "Custom"
        base = ConsolePrefixesInit(True, "good", "warn", "error", "green", "yellow", "red")

        first_prompt = True

        def mock_prompt(*args, **kwargs):
            nonlocal first_prompt
            if first_prompt:
                print(kwargs.pop("text"))
                first_prompt = False
                return 4
            else:
                return "Custom Answer"

        def mock_confirm(*args, **kwargs):
            return True

        monkeypatch.setattr(ConsolePrefixesInit, "prompt", mock_prompt)
        monkeypatch.setattr(ConsolePrefixesInit, "confirm", mock_confirm)

        # Act.
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        result = base.prompt_menu(question, answers, custom_answer)
        sys.stdout = sys.__stdout__  # Reset redirect.

        # Assert.
        assert result == "Custom Answer"
        assert captured_output.getvalue() == """Question?
  [0] Answer1
  [1] Answer2
  [2] Answer3
  [3] Answer4
  [4] Custom

"""
