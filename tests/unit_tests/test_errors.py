import sys
from io import StringIO
from pathlib import Path

import pytest

from todotree.Config.Config import Config
from todotree.Errors.ConfigFileNotFoundError import ConfigFileNotFoundError
from todotree.Errors.DoneFileNotFoundError import DoneFileNotFoundError
from todotree.Errors.GitError import GitError
from todotree.Errors.ProjectFolderError import ProjectFolderError
from todotree.Errors.TaskParseError import TaskParseError
from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError


class TestErrors:
    """Class that tests that the exceptions work."""

    def test_config_file_not_found(self):
        # Test that it can be raised.
        with pytest.raises(ConfigFileNotFoundError):
            raise ConfigFileNotFoundError("test")

        # Test that it can be raised from another error.
        with pytest.raises(ConfigFileNotFoundError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise ConfigFileNotFoundError("test") from e

        # Test that the echo and exit work.
        config = Config()
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        with pytest.raises(SystemExit):
            ConfigFileNotFoundError("test").echo_and_exit(config, Path("./testpath"), False)
        sys.stdout = sys.__stdout__

        assert captured_output.getvalue() == ''' ! The config.yaml file could not be found at testpath.
 ! The default options are now used.
'''

    def test_done_file_not_found(self):
        # Test that it can be raised.
        with pytest.raises(DoneFileNotFoundError):
            raise DoneFileNotFoundError("test")

        # Test that it can be raised from another error.
        with pytest.raises(DoneFileNotFoundError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise DoneFileNotFoundError("test") from e

        # Test that the echo and exit work.
        config = Config()
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        with pytest.raises(SystemExit):
            DoneFileNotFoundError("test").echo_and_exit(config)
        sys.stdout = sys.__stdout__

        assert captured_output.getvalue().startswith("!!!The done.txt could not be found.")

    def test_git_error(self):
        # Test that it can be raised.
        with pytest.raises(GitError):
            raise GitError("test")

        # Test that it can be raised from another error.
        with pytest.raises(GitError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise GitError("test") from e

        # Test that the echo and exit work.
        config = Config()
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        GitError("test").warn_and_continue(config.console)
        sys.stdout = sys.__stdout__

        assert captured_output.getvalue() == ''' ! An error occurred while trying to git pull.
 ! If this was unexpected, try again with the --verbose flag.
'''

    def test_project_folder_error(self):
        # Test that it can be raised.
        with pytest.raises(ProjectFolderError):
            raise ProjectFolderError("test")

        # Test that it can be raised from another error.
        with pytest.raises(ProjectFolderError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise ProjectFolderError("test") from e

    def test_task_parse_error(self):
        # Test that it can be raised.
        with pytest.raises(TaskParseError):
            raise TaskParseError("test")

        # Test that it can be raised from another error.
        with pytest.raises(TaskParseError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise TaskParseError("test") from e

    def test_todo_file_not_found(self):
        # Test that it can be raised.
        with pytest.raises(TodoFileNotFoundError):
            raise TodoFileNotFoundError("test")

        # Test that it can be raised from another error.
        with pytest.raises(TodoFileNotFoundError):
            try:
                raise FileNotFoundError
            except FileNotFoundError as e:
                raise TodoFileNotFoundError("test") from e

        # Test that the echo and exit work.
        config = Config()
        captured_output = StringIO()
        sys.stdout = captured_output  # Redirect stout.
        with pytest.raises(SystemExit):
            TodoFileNotFoundError("test").echo_and_exit(config)
        sys.stdout = sys.__stdout__

        assert captured_output.getvalue().startswith("!!!The todo.txt could not be found.")
