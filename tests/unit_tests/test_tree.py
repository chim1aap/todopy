from collections import OrderedDict

from todotree.Task.Task import Task
from todotree.Tree import Tree
from todotree.Config.TreePrint import TreePrint


class TestTree:
    """Test the tree structure"""

    def test_empty(self):
        """Test an empty tree structure"""
        # Act.
        result = Tree([], None)

        # Assert.
        assert result.data_structure == {}
        assert str(result) == ""

    def test_add_one(self):
        """Test adding an item to the tree"""
        # Arrange.
        task_list = [
            Task(1, "Test +projectName")
        ]

        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {"projectName": [task_list[0]]}

    def test_print_one(self):
        """Test adding an item to the tree"""
        # Arrange.
        task_list = [
            Task(1, "Test +projectName")
        ]

        # Act.
        result = Tree(task_list, "projects", treeprint=(TreePrint(".T.", ".L.", ".S.", "...")))

        # Assert.
        assert str(result) == """root
.S.projectName
....S.1 Test +projectName
"""

    def test_add_with_multiple_key(self):
        """Test adding an item who has multiple key values, such as with projects."""
        # Arrange.
        task_list = [
            Task(1, "Test +projectName +anotherProjectName")
        ]
        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {"projectName": [task_list[0]],
                                         "anotherProjectName": [task_list[0]]}

    def test_add_with_new_key(self):
        """Test adding an item who's key is a new unique value."""
        # Arrange.
        task_list = [
            Task(1, "Test +projectName"),
            Task(2, "Test +anotherProjectName")
        ]
        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {"projectName": [task_list[0]],
                                         "anotherProjectName": [task_list[1]]}

    def test_print_with_new_key(self):
        """Test adding an item who has multiple key values, such as with projects."""
        # Arrange.
        task_list = [
            Task(1, "Test +projectName"),
            Task(2, "Test +anotherProjectName")
        ]

        # Act.
        result = Tree(task_list, "projects", treeprint=(TreePrint(".T.", ".L.", ".S.", "...")))

        # Assert.
        assert str(result) == """root
.T.projectName
.L..S.1 Test +projectName
.S.anotherProjectName
....S.2 Test +anotherProjectName
"""

    def test_add_with_existing_key(self):
        """Test adding an item whose key is already in the tree"""
        # Arrange.
        task_list = [
            Task(1, "Test1 +projectName"),
            Task(2, "Test2 +projectName")
        ]

        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {"projectName": [task_list[0], task_list[1]]}

    def test_print_with_existing_key(self):
        """Test adding an item whose key is already in the tree"""
        # Arrange.
        task_list = [
            Task(1, "Test1 +projectName"),
            Task(2, "Test2 +projectName")
        ]

        # Act.
        result = Tree(task_list, "projects", treeprint=(TreePrint(".T.", ".L.", ".S.", "...")))

        # Assert.
        assert str(result) == """root
.S.projectName
....T.1 Test1 +projectName
....S.2 Test2 +projectName
"""

    def test_task_with_no_key(self):
        """Test the flow of a task where the key is the default."""
        # Arrange.
        task_list = [
            Task(1, "noProject")
        ]

        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {
            "default": [task_list[0]]
        }

    def test_happy_flow_complex(self):
        """Test adding a number of tasks, to test a real life situation."""
        # Arrange.
        task_list = [
            Task(0, "Test1 +projectName"),
            Task(1, "Test2 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 NoProject")
        ]
        # Act.
        result = Tree(task_list, "projects")

        # Assert.
        assert result.data_structure == {
            "projectName": [
                task_list[0],
                task_list[1]
            ],
            "anotherProject": [
                task_list[2]
            ],
            "default": [
                task_list[3]
            ]
        }

    def test_sort(self):
        """Test that sorting works in a simple case."""
        task_list = [
            Task(0, "Test2 +projectName"),
            Task(1, "Test1 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 NoProject")
        ]
        # Act.
        result: Tree = Tree(task_list, "projects").sort()

        # Assert.
        expected = OrderedDict()
        expected["default"] = [
            task_list[3]
        ]
        expected["projectName"] = [task_list[0],
                                   task_list[1]]
        expected["anotherProject"] = [task_list[2]]
        assert result.data_structure == expected

    def test_empty_sort(self):
        """Test that sorting also works on an empty Tree."""
        # Act.
        result: Tree = Tree([], "projects").sort()
        # Assert.
        assert result.data_structure == OrderedDict()

    def test_empty_default_sort(self):
        """Test that sorting works when the default case is empty."""
        task_list = [
            Task(0, "Test2 +projectName"),
            Task(1, "Test1 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 +anotherProject")
        ]
        # Act.
        result: Tree = Tree(task_list, "projects").sort()

        # Assert.
        expected = OrderedDict()
        expected["projectName"] = [task_list[0], task_list[1]]
        expected["anotherProject"] = [task_list[2], task_list[3]]
        assert result.data_structure == expected

    def test_sort_complex(self):
        """Test sorting a complex scenario"""
        task_list = [
            Task(0, "Test2 +projectName"),
            Task(1, "Test1 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 NoProject"),
            Task(4, "(B) Test5 NoProject"),
            Task(5, "Test5 +anotherProject"),
            Task(6, "(Z) Test6 +projectName"),
            Task(7, "Test7 NoProject"),
            Task(800, "(A) Test800 +projectName"),
            Task(9, "(M) Test9 +anotherProject"),
            Task(10, "(A) Test10 +projectName"),
        ]
        # Act.
        result: Tree = Tree(task_list, "projects").sort()

        # Assert.
        expected = OrderedDict()
        expected["default"] = [task_list[4], task_list[3], task_list[7]]
        expected["projectName"] = [task_list[10], task_list[8], task_list[6], task_list[0], task_list[1]]
        expected["anotherProject"] = [task_list[9], task_list[2], task_list[5]]
        assert result.data_structure == expected

    def test_stable_sorts(self):
        """Test that sorting is stable, ie. that running sort multiple times on the same list returns the same."""
        task_list = [
            Task(0, "Test2 +projectName"),
            Task(1, "Test1 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 NoProject"),
            Task(4, "(B) Test5 NoProject"),
            Task(5, "Test5 +anotherProject"),
            Task(6, "(Z) Test6 +projectName"),
            Task(7, "Test7 NoProject"),
            Task(800, "(A) Test800 +projectName"),
            Task(9, "(M) Test9 +anotherProject"),
            Task(10, "(A) Test10 +projectName"),
        ]
        expected = OrderedDict()
        expected["default"] = [task_list[4], task_list[3], task_list[7]]
        expected["projectName"] = [task_list[10], task_list[8], task_list[6], task_list[0], task_list[1]]
        expected["anotherProject"] = [task_list[9], task_list[2], task_list[5]]

        # Act & Asserts.
        result: Tree = Tree(task_list, "projects").sort()
        assert result.data_structure == expected
        result2: Tree = Tree(task_list, "projects").sort()
        assert result2.data_structure == expected
        assert result.data_structure == result2.data_structure

    def test_print_happy_flow_complex(self):
        """Test adding a number of tasks, to test a real life situation."""
        # Arrange.
        task_list = [
            Task(5, "Test1 +projectName"),
            Task(1, "Test2 +projectName"),
            Task(2, "Test3 +anotherProject"),
            Task(3, "Test4 NoProject")
        ]

        # Act.
        result = Tree(task_list, "projects", treeprint=(TreePrint(".T.", ".L.", ".S.", "...")))

        # Assert.
        assert str(result) == """root
.T.projectName
.L..T.5 Test1 +projectName
.L..S.1 Test2 +projectName
.T.anotherProject
.L..S.2 Test3 +anotherProject
.S.default
....S.3 Test4 NoProject
"""

    def test_happy_flow_dates(self):
        """Test adding a number of tasks and make a tree by their due date."""
        # Arrange.
        task_list = [
            Task(0, "Test1 due:2023-01-01"),
            Task(1, "Test2 due:2023-01-01"),
            Task(2, "Test3 due:2023-02-02"),
            Task(3, "Test4 No due date")
        ]

        # Act.
        result = Tree(task_list, "due_date")

        # Assert.
        assert result.data_structure == {
            task_list[0].due_date: [
                task_list[0],
                task_list[1]
            ],
            task_list[2].due_date: [
                task_list[2]
            ],
            "default": [
                task_list[3]
            ]
        }
