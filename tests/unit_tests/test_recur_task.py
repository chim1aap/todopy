import datetime

import pytest

from todotree.Errors.RecurParseError import RecurParseError
from todotree.Task.RecurTask import RecurTask


class TestRecurTask:
    """Test the recur_task class."""

    def test_init(self):
        """Test that the recur_task can initialize on an empty line."""
        RecurTask("   ")

    def test_parse(self):
        """Test that the recur_task can parse a line."""
        result = RecurTask("2020-01-01 ; 2023-01-01 ; daily - (A) Test Task ")

        assert result.start_date == datetime.date(2020, 1, 1)
        assert result.end_date == datetime.date(2023, 1, 1)
        assert result.task_string == "(A) Test Task"
        assert result.interval == RecurTask.Recurrence.Daily

    def test_parse_no_end_date(self):
        """Test that parsing works when no end date is provided."""
        result = RecurTask("2020-01-01 ;            ; daily - (A) Test Task ")

        assert result.start_date == datetime.date(2020, 1, 1)
        assert result.end_date == datetime.date(9999, 1, 1)
        assert result.task_string == "(A) Test Task"
        assert result.interval == RecurTask.Recurrence.Daily

    def test_invalid_start_date(self):
        """Test that an invalid start date raises an error."""
        with pytest.raises(RecurParseError, match="Error parsing start date"):
            RecurTask("2020-11-41 ;            ; daily - (A) Test Task ")

    def test_invalid_interval(self):
        """Test that an invalid interval raises an error."""
        with pytest.raises(RecurParseError, match="Error parsing interval"):
            RecurTask("2020-11-01 ; 2023-01-01 ; err - (A) Test Task")

    def test_invalid_end_date(self):
        """Test that an invalid end date raises an error."""
        with pytest.raises(RecurParseError, match="Error parsing end date:*"):
            RecurTask("2020-11-01 ; 2023-30-01 ; daily - (A) Test Task ")

    def test_invalid(self):
        """Test that an invalid string raises an error."""
        with pytest.raises(RecurParseError, match="This string does not match regex."):
            RecurTask("2020-01-01 ; 2023-01-01 ; daily ; (A) Test Task ")
