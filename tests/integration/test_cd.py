from click.testing import CliRunner
from todotree.Main import root

from tests.helper import setup_files


class TestCd:
    def test_cd(self, monkeypatch):
        """Test the cd command"""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "cd")
        assert result.exit_code == 0
        assert "xdg/data/todotree" in result.output