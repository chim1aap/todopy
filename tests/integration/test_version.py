from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


class TestMainVerbose:
    """
    Test the --verbose and --quiet options.

    This test uses the version command. It breaks if you do not have todotree installed in the venv.
    But that should not be a problem in the CI/CD system, as that one should never have it already installed.
    This can be fixed locally by installing the dev build via `pip install -e .`.
    """

    def test_both(self, monkeypatch):
        """Test both options. (--quiet should have precedence). """
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "--verbose --quiet version")
        assert result.exit_code == 0
        assert "99.0.0" in result.output
        assert "version" not in result.output, "Verbose was on."
        assert "Version" not in result.output, "Quiet was off."

    def test_quiet(self, monkeypatch):
        """Test the --quiet option"""
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, " --quiet version")

        assert "99.0.0" in result.output
        assert "version" not in result.output, "Verbose was on."
        assert "Version" not in result.output, "Quiet was off."

    def test_verbose(self, monkeypatch):
        """Test the --verbose option before the command."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "--verbose version")
        assert result.exit_code == 0
        assert "The version is" in result.output

    def test_verbose_after(self, monkeypatch):
        """Test the --verbose option after the command."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "version --verbose")
        assert result.exit_code == 0
        assert "The version is" in result.output

    def test_combined(self, monkeypatch):
        """Test that options before still exists when parsing options after the command."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "--quiet version --verbose")
        assert result.exit_code == 0
        assert "99.0.0" in result.output
        assert "version" not in result.output, "Verbose was on."
        assert "Version" not in result.output, "Quiet was off."

    def test_default(self, monkeypatch):
        """Test the default option."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "version")
        assert result.exit_code == 0
        assert "99.0.0" in result.output
        assert "version" not in result.output
        assert "Version" in result.output
