from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root, cli


class TestStale:
    """Integration tests of the stale system."""

    def test_help(self):
        """Test that the help works"""
        runner = CliRunner()
        result = runner.invoke(root, "stale")
        result2 = runner.invoke(root, "stale add --help")
        assert result.exit_code == 0
        assert "Commands:" in result.output
        assert "Options:" in result.output
        assert " stale [OPTIONS]" in result.output

    def test_list_args(self, monkeypatch):
        """Test that the list function works."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, donetxt, *_ = setup_files(monkeypatch, todotxt_content="", donetxt_content="",
                                               stale_content="First stale task\nSecond stale task")
            result = runner.invoke(root, "stale list --verbose")
        print(result.output)
        assert result.exit_code == 0
        assert "1 First stale task" in result.output
        assert "2 Second stale task" in result.output

    def test_add_args(self, monkeypatch):
        """Test that we can move a task from the main list to stale.txt"""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Test Task\n Task to Move\nThird Task",
                                                donetxt_content="",
                                                stale_content="First stale task\nSecond stale task\n")
            result = runner.invoke(root, "stale add 2 --verbose")
            # Assert.
            todotxt_content = todotxt.read_text()
            staletxt_content = staletxt.read_text()

            print(result.output)
            assert result.exit_code == 0
            assert "Test Task" in todotxt_content
            assert "Task to Move" in staletxt_content
            assert "Third Task" in todotxt_content

    def test_revive_args(self, monkeypatch):
        """Test that we can move a task from stale.txt to the main list."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", donetxt_content="",
                                                stale_content="First stale task\nSecond stale task\n Third stale task\n")
            # Act.
            result = runner.invoke(root, "stale revive 2 --verbose")
            # Assert.
            todotxt_content = todotxt.read_text()
            staletxt_content = staletxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "Task One" in todotxt_content
            assert "Task Two" in todotxt_content
            assert "Second stale task" in todotxt_content
            assert "First stale task" in staletxt_content
            assert "Third stale task" in staletxt_content

    def test_list(self, monkeypatch):
        """Test that the list function works."""
        runner = CliRunner()

        with runner.isolated_filesystem():
            todotxt, donetxt, *_ = setup_files(monkeypatch, todotxt_content="", donetxt_content="",
                                               stale_content="First stale task\nSecond stale task")
            result = runner.invoke(root, "stale list")
        print(result.output)
        assert result.exit_code == 0
        assert "1 First stale task" in result.output
        assert "2 Second stale task" in result.output

    def test_add(self, monkeypatch):
        """Test that we can move a task from the main list to stale.txt"""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt,  *_, staletxt = setup_files(monkeypatch, todotxt_content="Test Task\n Task to Move\nThird Task", donetxt_content="",
                                               stale_content="First stale task\nSecond stale task\n")
            # Act.
            result = runner.invoke(root, "stale add 2")

            # Assert.
            todotxt_content = todotxt.read_text()
            staletxt_content = staletxt.read_text()

            print(result.output)
            assert result.exit_code == 0
            assert "Test Task" in todotxt_content
            assert "Task to Move" in staletxt_content
            assert "Third Task" in todotxt_content

    def test_revive(self, monkeypatch):
        """Test that we can move a task from stale.txt to the main list."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", donetxt_content="",
                                               stale_content="First stale task\nSecond stale task\n Third stale task\n")
            result = runner.invoke(root, "stale revive 2")
            todotxt_content = todotxt.read_text()
            staletxt_content = staletxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "Task One" in todotxt_content
            assert "Task Two" in todotxt_content
            assert "Second stale task" in todotxt_content
            assert "First stale task" in staletxt_content
            assert "Third stale task" in staletxt_content

    def test_stale_list_not_found(self, monkeypatch):
        """Test stale list when stale.txt is not found."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", donetxt_content="")
            result = runner.invoke(root, "stale list")
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "The stale.txt file could not be found." in result.output

    def test_stale_revive_not_found(self, monkeypatch):
        """Test stale revive when stale.txt is not found."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", donetxt_content="")
            result = runner.invoke(root, "stale revive 2")
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "The stale.txt file could not be found." in result.output

    def test_stale_add_not_found(self, monkeypatch):
        """Test stale add when stale.txt is not found."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", donetxt_content="")
            result = runner.invoke(root, "stale add 2")
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "The stale.txt file could not be found." in result.output

    def test_stale_add_task_not_found(self, monkeypatch):
        """Test stale add when the task does not exist."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", stale_content="1\n")
            result = runner.invoke(root, "stale add 200")
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "Task 200 does not exist in" in result.output
            assert "todo.txt" in result.output

    def test_stale_revive_task_not_found(self, monkeypatch):
        """Test stale add when the task does not exist."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            todotxt, *_, staletxt = setup_files(monkeypatch, todotxt_content="Task One\nTask Two\n", stale_content="1\n2\n3\n4\n5")
            result = runner.invoke(root, "stale revive 200")
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "does not exist in" in result.output
            assert "stale.txt" in result.output
