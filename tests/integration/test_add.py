from click.testing import CliRunner

from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError
from todotree.Main import root
from todotree.Managers.AbstractManager import AbstractManager
from tests.helper import setup_files


class TestAdd:

    def test_add(self, monkeypatch):
        """Test the add command"""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="Existing Task\n")
            # Act.
            result = runner.invoke(root, ' add "New Task"')
            print(result)

            # Assert.
            assert result.exit_code == 0
            assert todotxt.read_text() == "Existing Task\nNew Task\n"

    def test_add_nargs(self, monkeypatch):
        """Test the add command with multiple words (instead of using "quotes")."""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="Existing Task\n")

            # Act.
            result = runner.invoke(root, ' add New Task')

            # Assert.
            assert result.exit_code == 0
            assert todotxt.read_text() == "Existing Task\nNew Task\n"

    def test_add_empty(self, monkeypatch):
        """Test the add command with empty task."""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="Existing Task\n")

            # Act.
            result = runner.invoke(root, ' add ')

            # Assert.
            assert result.exit_code == 1
            assert todotxt.read_text() == "Existing Task\n"

    def test_add_empty_todotxt(self, monkeypatch):
        """Test that we can add a task when the list is empty. See #61."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="")
            result = runner.invoke(root, ' add "New Task"')
            assert result.exit_code == 0
            assert todotxt.read_text() == "New Task\n"

    def test_no_file_found_add(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `add`.
        """

        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "add 'new task'")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_add_sparse(self, monkeypatch):
        """Test the add command"""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="Existing Task\n\n\nAnother\n")
            # Act.
            result = runner.invoke(root, ' add "New Task"')
            print(result)

            # Assert.
            assert result.exit_code == 0
            assert todotxt.read_text() == "Existing Task\nNew Task\n\nAnother\n"