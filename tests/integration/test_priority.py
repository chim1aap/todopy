from click.testing import CliRunner
from todotree.Managers.AbstractManager import AbstractManager

from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError

from todotree.Main import root
from tests.helper import setup_files


class TestPriority:
    """Integration tests for the priority command."""

    def test_priority(self, monkeypatch):
        """Test the priority command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' priority 2 B')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == 'Task which is unfinished\n(B) Task which is marked\nTask which is unfinished\n'
            assert done_file.read_text() == done_content

    def test_priority_invalid(self, monkeypatch):
        """Test the priority command with invalid entries"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' priority 2 cv')

            # Assert.
            print(result.output)
            assert result.exit_code == 1
            assert "CV is not a valid priority." in result.output
            # Files are not changed.
            assert todo_file.read_text() == todo_content
            assert done_file.read_text() == done_content

    def test_no_file_found_priority(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `priority`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "priority 1 a")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_spare_files_priority(self, monkeypatch):
        """Test sparse files in the priority command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\n\n\n\nTask which is marked\n\n\n\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' priority 5 B')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == 'Task which is unfinished\n\n\n\n(B) Task which is marked\n\n\n\nTask which is unfinished\n'
            assert done_file.read_text() == done_content
