from pathlib import Path

import pytest
from click.testing import CliRunner

from todotree.Commands.Init import Init
from todotree.Config.Config import Config
from todotree.Main import root


class TestInit:
    """Test the init configuration system."""

    @pytest.mark.timeout(5)
    def test_yes_to_all(self, monkeypatch):
        """Test the scenario when you answer yes and the first option."""
        runner = CliRunner()

        def mock_writer(self, *args, **kwargs):
            # This is tested when setting custom paths.
            pass

        monkeypatch.setattr(Config, "write_config", mock_writer)
        monkeypatch.setattr(Init, "write_example", mock_writer)
        flow = [
            # Console Prefixes.
            "y",
            "0",
            "y",
            # Config location.
            "0",
            "y",
            # Folder location.
            "0",
            "y",
            # Git.
            "y",
            "y",
            # Project
            "y",
            "0",
            # example.
            "y", "y", "y", "y",
            # Go.
            "y",
            "y",
        ]

        runner_input = "\n".join(flow) + "\n"
        result = runner.invoke(root, "init", input=runner_input)
        print(result.output)
        assert "Error: invalid input" not in result.output, "Input error."
        assert "not valid" not in result.output
        assert result.exit_code == 0
        assert "Written the files." in result.output

    @pytest.mark.timeout(5)
    def test_custom_to_all(self, monkeypatch):
        """
        Enable all features, but custom answers where possible.
        Also tests that example files are written to the locations.
        """
        runner = CliRunner()

        with runner.isolated_filesystem():
            flow = [
                # Console Prefixes.
                "n",
                "1",
                "y",
                # Config location.
                "2",
                "./config.yaml",
                "y",
                # Folder location.
                "2",
                "./",
                "y",
                # Git.
                "y",
                "y",
                # Project
                "y",
                "./projects",
                "y",
                # example.
                "y", "y", "y", "y",
                # Go.
                "y",
            ]

            runner_input = "\n".join(flow) + "\n"
            result = runner.invoke(root, "init", input=runner_input)
            print(result.output)

            assert "Error: invalid input" not in result.output, "Input error."
            assert "not valid" not in result.output
            assert "You need to supply todotree each time" in result.output
            assert result.exit_code == 0

            config_content = Path("config.yaml").read_text()
            todotxt_content = Path("todo.txt").read_text()
            stale_content = Path("stale.txt").read_text()
            recur_content = Path("recur.txt").read_text()

            # Output files.
            assert config_content is not None
            assert " * " in config_content
            assert "enable_project_folder: true" in config_content
            assert "enable_colors: false" in config_content
            assert todotxt_content is not None
            assert "(A) This is an example todo.txt file" in todotxt_content
            assert "(A) This is an example stale.txt file" in stale_content
            assert "This is an example recur.txt file" in recur_content

    @pytest.mark.timeout(5)
    def test_unsure_user(self, monkeypatch):
        """Test each time a user will say No once on the confirmation prompts."""
        runner = CliRunner()

        def mock_writer(self, *args, **kwargs):
            # This is tested when setting custom paths.
            pass

        monkeypatch.setattr(Config, "write_config", mock_writer)
        monkeypatch.setattr(Init, "write_example", mock_writer)
        flow = [
            # Console Prefixes.
            "y",
            "0",
            "n",
            "0",
            "y",
            # Config location.
            "0",
            "n",
            "0",
            "y",
            # Folder location.
            "0",
            "n",
            "0",
            "y",
            # Git.
            "y",
            "y",
            # Project
            "y",
            "./",
            # example.
            "y", "y", "y", "y",
            # Go.
            "y",
            "y",
        ]

        runner_input = "\n".join(flow) + "\n"
        result = runner.invoke(root, "init", input=runner_input)
        print(result.output)
        assert "Error: invalid input" not in result.output, "Input error."
        assert "not valid" not in result.output
        assert result.exit_code == 0
