import datetime
from unittest import skip

from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


# noinspection PyTypeChecker
class TestSparse:
    """
    Integration tests for files with sparse todotxt.

    FUTURE: Migrate to separate integration test classes.
    """
    yaml_content = """
git:
    mode: disabled"""


    def test_append(self, monkeypatch):
        """Test the append command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Existing Task\n\n\n\nIncomplete Task\n\n\n\nExisting Task\n"
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content, donetxt_content="")
            # Act.
            result = runner.invoke(root, ' append 5 "New finished Task"')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == "Existing Task\n\n\n\nIncomplete Task New finished Task\n\n\n\nExisting Task\n"

    def test_do(self, monkeypatch):
        """Test the do command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\n\n\n\nTask which is marked\n\n\n\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' do 5')

            # Assert.
            print(result.output)
            assert result.exit_code == 0
            assert todo_file.read_text() == "Task which is unfinished\n\n\n\n\n\n\n\nTask which is unfinished\n"
            assert done_file.read_text() == "x 2020-01-01 Done task\nx " + str(datetime.datetime.today().date()) \
                   + " Task which is marked\n"

    def test_revive(self, monkeypatch):
        """Test the revive command."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\n\n\n\nTask which is unfinished\n\n\n\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\nx 2023-07-10 Another Done Task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)

            # Act.
            result = runner.invoke(root, ' revive 2')
            # Assert.
            print(result.stdout)

            assert result.exit_code == 0
            done_file_read_text = done_file.read_text()
            assert done_file_read_text == "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\nx 2023-07-10 Another Done Task\n"
            assert todo_file.read_text() == "Task which is unfinished\nTask which is marked\n\n\nTask which is unfinished\n\n\n\nTask which is unfinished\n"

    def test_schedule(self, monkeypatch):
        """Test the schedule command."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task scheduled at \n\n\n\nTask which is unfinished\n\n\n\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content, configyaml_content=self.yaml_content)
            # Act.
            result = runner.invoke(root, ' schedule 5 "2025-01-01"')

            # Assert.
            assert result.exit_code == 0
            assert done_file.read_text() == done_content
            assert todo_file.read_text() == "Task scheduled at\n\n\n\nTask which is unfinished t:2025-01-01\n\n\n\nTask which is unfinished\n"
