from pathlib import Path

import xdg_base_dirs as xdg
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Config.Config import Config
from todotree.Config.ConsolePrefixes import ConsolePrefixes
from todotree.Config.TreePrint import TreePrint
from todotree.Errors.GitError import GitError


class TestConfig:
    """Test the configuration system."""

    # This yaml should be exactly as the example.yaml in the documentation.
    # Even the spaces.
    different_yaml = """# This config file shows all the options and their defaults.
# It also has documentation for all the options.

main:
  # A boolean indicating whether to print more detailed messages.
  verbose: true

  # A boolean indicating whether to print anything except the output. Useful in scripts.
  # Overrides verbose.
  quiet: true

  # A value indicating whether to enable the project folder functionality.
  enable_project_folder: false

paths:
  # Path to the folder containing the data files.
  # Relative paths are calculated from the HOME folder.
  folder: different

  # Path to the todo.txt file.
  todo_file: different

  # Path to the done.txt file.
  done_file: different

  # Path to the folder containing the projects (if enabled).
  project_folder: different

  # Path to the folder containing the addons.
  addons_folder: different

localization:
  # Name of the wishlist project.
  wishlist_name: different

  # Name of the "empty" project for tasks that do not have any project.
  no_project: different

  # Name of the "empty" context for tasks that do not have any context.
  no_context: different

  # Task string to add when a project does not have tasks.
  # This can only happen if the project functionality is enabled.
  empty_project: different

decorators:
  # Whether to decorate the prefixes with colors.
  enable_colors: false

  # Prefix of the messages containing information.
  info: different

  # Prefix of the messages containing a warning.
  warning: different

  # Prefix of the messages containing an error.
  error: different

  # Color of the info prefix (if enabled).
  info_color: blue

  # Color of the warning prefix (if enabled).
  warning_color: blue

  # Color of the error prefix (if enabled).
  error_color: blue

# Elements needed for printing the tree.
# Change these if the tree looks wrong, for example if the terminal cannot support UTF-8 characters.
# The quotes are needed so that each element is exactly the same length,
# otherwise the tree looks untidy.
tree:
  # t junction.
  t: different
  # l piece.
  l: different
  # s .
  s: different
  # empty.
  e: different

git:
  # The mode that git runs in.
  # - disabled: disables it,
  # - Local: add and commits automatically,
  # - Full: also pulls and pushes to a remote repo.
  mode: full

  # The amount of time in minutes until todotree pulls the remote repo again.
  # This is to stop fetching each time you run a command.
  # Set to 0 to disable this behavior.
  pull_delay: 0
"""

    def test_init(self):
        """Test Config can initialize and that some properties are set."""
        # Arrange and Act.
        config = Config()

        # Assert.
        # Default home directories (These are slightly non-trivial).
        assert config.paths.project_tree_folder == xdg.xdg_data_home() / "todotree" / "projects"
        assert config.paths.todo_file == xdg.xdg_data_home() / "todotree" / "todo.txt"
        assert config.paths.done_file == xdg.xdg_data_home() / "todotree" / "done.txt"

    def test_can_read_from_empty_yaml(self):
        """Test that an empty config file does not change properties."""
        # Arrange.
        config_read_from_file = Config()

        # Act.
        config_read_from_file._read_from_yaml("")

        # Assert.
        self.__assert_object(Config(), config_read_from_file, equal=True, ignored_items="config_file")

    def test_can_read_from_yaml_with_only_headings(self):
        """
        Test that a config file which only has top tree items does not change properties.

        So it has content, but all information results in just no-op.
        """
        # Arrange.
        config_read_from_file = Config()

        # Act.
        config_read_from_file._read_from_yaml("""
main: # Empty section.
tree:
    DoesNotExistItem: True # Item which does not exist.
taskmanager:        # Section which is not used.
    """)

        # Assert.
        self.__assert_object(Config(), config_read_from_file, equal=True, ignored_items="config_file")

    def test_example_yaml_equals_defaults(self):
        """
        Test that importing settings from the example yaml
        yield the same as if there was no configuration file.
        Therefor, the example file correctly documents the defaults"""
        # Arrange.
        config_from_file = Config()

        # Act.
        config_from_file.read_from_file(Path("../todotree/examples/config.yaml"))

        # Assert.
        self.__assert_object(Config(), config_from_file, equal=True, ignored_items=["config_file"])

    def test_that_all_settings_can_be_changed(self):
        """
        Test that each property of the config file can be configured by the yaml file.
        """
        # Arrange.
        config_read_from_file = Config()

        # Act.
        config_read_from_file._read_from_yaml(self.different_yaml)

        # Assert.
        self.__assert_object(Config(), config_read_from_file, equal=False, ignored_items=["config_file"])

    def test_config_write_default(self, monkeypatch):
        """Test that the default config writes a default config file to the location."""
        runner = CliRunner()
        config = Config()
        result_example = Path("../todotree/examples/config.yaml").read_text()

        with runner.isolated_filesystem() as temp_dir:
            setup_files(monkeypatch)
            config.config_file = Path(temp_dir) / "config.yaml"
            config.write_config()
            result_contents = (Path(temp_dir) / "config.yaml").read_text()
            new_read_config = Config()
            new_read_config.read(Path(temp_dir) / "config.yaml")

        print(result_contents)
        print(result_example)
        # test comments.
        s = result_contents.splitlines()
        for number, line in enumerate(result_example.splitlines()):
            if line.strip().startswith("#"):
                assert s[number] == line

        # Test config.
        fresh_config = Config()
        self.__assert_object(
            fresh_config,
            new_read_config,
            equal=True,
            ignored_items=["config_file", "_Config__yaml_object", "paths", "git"],
        )

    def test_config_write_different(self, monkeypatch):
        """Test that each option can be written in the resulting config file."""
        # Arrange.
        runner = CliRunner()
        config = Config()

        # Change each setting.
        with runner.isolated_filesystem() as temp_dir:
            setup_files(monkeypatch)
            config.paths.todo_folder = Path("different")
            config.paths.project_tree_folder = Path("different")
            config.paths.todo_file = Path("different")
            config.paths.done_file = Path("different")
            config.paths.recur_file = Path("different")
            config.paths.addons_folder = Path("different")
            config.enable_project_folder = False
            config.wishlistName = "different"
            config.noProjectString = "different"
            config.noContextString = "different"
            config.emptyProjectString = "different"
            config.console = ConsolePrefixes(False, "different", "different", "different", "blue", "blue", "blue")
            config.tree_print = TreePrint("different", "different", "different", "different")
            try:
                config.git.git_mode = "Full"
            except GitError:
                pass
            config.git.pull_time_minutes = 0
            config.git.todo_folder = Path("different")
            config.console.set_verbose()

            # Act.
            config.config_file = Path(temp_dir) / "config.yaml"
            config.write_config()

            # Assert.
            result_contents = (Path(temp_dir) / "config.yaml").read_text()
            new_read_config = Config()
            new_read_config.read(Path(temp_dir) / "config.yaml")

        print(result_contents)
        # test comments.
        result_example = Path("../todotree/examples/config.yaml").read_text()
        s = result_contents.splitlines()
        for number, line in enumerate(result_example.splitlines()):
            if line.strip().startswith("#"):
                assert s[number] == line

        # Test config.
        fresh_config = Config()
        self.__assert_object(fresh_config, new_read_config, equal=False)
        self.__assert_object(config, new_read_config, equal=True)

    @staticmethod
    def __assert_object(left_object, right_object, equal, ignored_items=None):
        """Test properties (un)equality of left and right objects. Ignore keys in `ignored_items`."""
        if ignored_items is None:
            ignored_items = []
        for name, _ in vars(left_object).items():
            if name in ignored_items:
                print(f"Skipped {name}.")
                continue
            if "__" in name:
                print(f"Skipped private variable {name}")
                continue
            print(name, getattr(left_object, name), getattr(right_object, name))
            if equal:
                assert getattr(left_object, name) == getattr(right_object, name)
            else:
                assert getattr(left_object, name) != getattr(right_object, name)
