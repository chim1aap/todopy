#!/usr/bin/python
import argparse

p = argparse.ArgumentParser()
p.add_argument('--more', action='store_true')
p.add_argument("todo_file")

args = p.parse_args()

if args.more:
    print("More ", end='')
print("Hello World!")
