class PoemAddon:
    def __init__(self):
        pass

    def poem(self):
        print("""
        I don't like poems.
        They feel to me
        like how code must feel
        to non-coders.
        Like a puzzle
        that's not meant to be solved
        but just look fancy.
        Vague and wobbly lines
        that break randomly,
        like code with random 
            indentations.
        The only poem I understand
        is pip install poetry.
        """)


if __name__ == '__main__':
    PoemAddon().poem()
