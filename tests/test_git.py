from click.testing import CliRunner

from tests.helper import setup_git, setup_git_local_only, setup_files
from todotree.Main import root


class TestGit:
    """Integration test of Git functionality."""

    def test_commit(self, monkeypatch):
        """Test the flow when only commits are added automatically."""
        runner = CliRunner()

        with runner.isolated_filesystem() as remote:
            with runner.isolated_filesystem():
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""git:
                                                        mode: Local""")
                setup_git(remote, todo_file.parent)

                # Act.
                result = runner.invoke(
                    root, f"add 'new task'"
                )

                # Assert.
                print(result.output)
                assert result.exit_code == 0
                assert "Commit added" in result.output
                assert "Push successful" not in result.output

    def test_commit_failed_repo(self, monkeypatch):
        """Test the flow when the repo is not initialized yet."""
        runner = CliRunner()

        with runner.isolated_filesystem():
            with runner.isolated_filesystem():
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""git:
                  mode: Local
                """)

                # Act.
                result = runner.invoke(
                    root, f"list"
                )

                # Assert.
                assert result.exit_code == 1
                assert result.output.startswith("!!!Git repository is not initialized.")

    def test_full(self, monkeypatch):
        """Test the flow when also pushing and pulling is enabled."""
        runner = CliRunner()
        with runner.isolated_filesystem() as remote:
            with runner.isolated_filesystem():
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""
                git:
                  mode: Full
                """)
                setup_git(remote, todo_file.parent)

                # Act.
                result = runner.invoke(
                    root, f"add 'New task to add'"
                )

                # Assert.
                print(result.output)
                assert result.exit_code == 0
                assert "Commit added" in result.output
                assert "Push successful" in result.output

    def test_full_failed_repo(self, monkeypatch):
        """Test the flow when the repo is not initialized."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            with runner.isolated_filesystem():
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""
                git:
                  mode: Full
                """)
                # Act.
                result = runner.invoke(
                    root, f"add 'New task to add'"
                )

                # Assert.
                assert result.exit_code == 1
                assert result.output.startswith("!!!Git repository is not initialized.")

    def test_full_upstream_missing(self, monkeypatch):
        """Test the flow when the current branch does not have an upstream branch defined."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            with runner.isolated_filesystem():
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""git:
                                                        mode: Full""")
                setup_git_local_only(todo_file.parent)
                # Act.
                result = runner.invoke(
                    root, f"add 'New task to add'"
                )

                # Assert.
                assert result.exit_code == 1
                assert "The remote likely does not exist" in result.output

    def test_full_timeout(self, monkeypatch):
        """Test that git does not pull if pulled too recently."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            with runner.isolated_filesystem() as remote:
                todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content="Test Task", donetxt_content="",
                                                      configyaml_content=f"""git:
                  mode: Full
                """)
                setup_git(remote, todo_file.parent)

                # Act.
                result_with_pull = runner.invoke(
                    root, f"list"
                )
                # This list action should not pull, because above line finishes in less than a minute.
                result_without_pull = runner.invoke(
                    root, f"list"
                )
                # Assert.
                print(result_with_pull.output)
                print(result_without_pull.output)
                assert result_with_pull.exit_code == 0
                assert result_without_pull.exit_code == 0
                assert "Pulling latest changes" in result_with_pull.output
                assert "Pulling latest changes" not in result_without_pull.output
