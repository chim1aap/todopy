import datetime

import click
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


# noinspection PyTypeChecker
class TestMainIntegrationEdit:
    """Test endpoints which edit."""
    yaml_content = """
git:
    mode: disabled"""

    def test_add_x(self, monkeypatch):
        """Test the addx command"""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Arrange.
            todotxt, donetxt, *_ = setup_files(monkeypatch, donetxt_content="Existing Task\n")

            # Act.
            result = runner.invoke(root, ' addx "New finished Task"')

            # Assert.
            print(result.output)
            assert result.exit_code == 0
            assert donetxt.read_text() == "Existing Task\nx " + str(
                datetime.datetime.today().date()) + " New finished Task\n"

    def test_append(self, monkeypatch):
        """Test the append command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Existing Task\nIncomplete Task\nExisting Task\n"
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content, donetxt_content="")
            # Act.
            result = runner.invoke(root, ' append 2 "New finished Task"')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == "Existing Task\nIncomplete Task New finished Task\nExisting Task\n"

    def test_append_nargs(self, monkeypatch):
        """Test the append command  with multiple words (instead of using "quotes")."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Existing Task\nIncomplete Task\nExisting Task\n"
        with runner.isolated_filesystem():
            # Arrange - add task file with existing todos.
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content, donetxt_content="")

            # Act.
            result = runner.invoke(root, ' append 2 New finished Task')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == "Existing Task\nIncomplete Task New finished Task\nExisting Task\n"

    def test_do(self, monkeypatch):
        """Test the do command"""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' do 2')

            # Assert.
            print(result.output)
            assert result.exit_code == 0
            assert todo_file.read_text() == "Task which is unfinished\n\nTask which is unfinished\n"
            assert done_file.read_text() == "x 2020-01-01 Done task\nx " + str(datetime.datetime.today().date()) \
                   + " Task which is marked\n"

    def test_dont(self, monkeypatch):
        """Test the do command when one or more are non existing tasks."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' do 1 2 3 4 5')

            # Assert.
            print(result.output)
            assert result.exit_code == 1

            assert todo_file.read_text() == todo_content
            assert done_file.read_text() == done_content

    def test_revive(self, monkeypatch):
        """Test the revive command."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is unfinished\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\nx 2023-07-10 Another Done Task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)

            # Act.
            result = runner.invoke(root, ' revive 2')
            # Assert.
            print(result.stdout)

            assert result.exit_code == 0
            done_file_read_text = done_file.read_text()
            assert done_file_read_text == done_content
            assert todo_file.read_text() == todo_content + "Task which is marked\n"

    def test_schedule(self, monkeypatch):
        """Test the schedule command."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task scheduled at \nTask which is unfinished\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content, configyaml_content=self.yaml_content)
            # Act.
            result = runner.invoke(root, ' schedule 1 "2025-01-01"')

            # Assert.
            assert result.exit_code == 0
            assert done_file.read_text() == done_content
            assert todo_file.read_text() == "Task scheduled at t:2025-01-01\nTask which is unfinished\nTask which is unfinished\n"
