from todotree.Errors.TodotreeError import TodotreeError


class TaskParseError(TodotreeError):
    """
    Represents an error when parsing a task.
    """
    pass

