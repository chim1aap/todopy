from todotree.Errors.TodotreeError import TodotreeError


class ProjectFolderError(TodotreeError):
    """
    Represents an error when parsing the project folder.
    """

    pass
